﻿namespace RouterPlacement.Enums
{
    enum CellType
    {
        Wall='#',
        Target = '.',
        Void = '-'
    }
}
