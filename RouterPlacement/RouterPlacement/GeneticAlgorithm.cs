﻿using RouterPlacement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RouterPlacement
{
    class GeneticAlgorithm
    {
        readonly int p;
        readonly int t;
        readonly int mutationRate;
        readonly int iterations;

        public List<Solution> Populate { get; set; }

        public GeneticAlgorithm(int p, int t, int mutationRate, int iterations)
        {
            this.p = p;
            this.t = t;
            this.mutationRate = mutationRate;
            this.iterations = iterations;
            this.Populate = new List<Solution>();
        }

        public Solution FindSolution()
        {
            this.GeneratePopulation();
            var parent = this.TournamentSelection();
            Random rand = new Random();

            for (int i = 0; i < this.iterations; i++)
            {
                var mutant = parent.Copy();

                var randomNr = rand.Next(0, 100);
                if (this.mutationRate > randomNr)
                {
                    mutant.Mutate();
                }

                this.RepalceWorst(mutant);
            }

            return this.GetBest();
        }

        private void RepalceWorst(Solution mutant)
        {
            var worstIndividual = this.GetWorst();

            if (worstIndividual.GetScore() < mutant.GetScore())
            {
                Console.WriteLine(mutant.GetScore());
                this.Populate.Remove(worstIndividual);
                this.Populate.Add(mutant);
            }
        }

        private void GeneratePopulation()
        {
            for (int i = 0; i < this.p; i++)
            {
                Populate.Add(Solution.GenerateRandom());
            }
        }

        private Solution RouleteWheelSelect()
        {
            double totalScore = Populate.Sum(x => x.GetScore());
            var probabilitySlots = new List<ProbabilitySlotSoltion>();
            double slotSizeFactor = 100000;

            foreach (var individual in Populate)
            {
                double propability = individual.GetScore() / totalScore;

                int slotSize = (int)(propability * slotSizeFactor);
                var lastSlot = probabilitySlots.LastOrDefault();

                if (lastSlot != null)
                {
                    probabilitySlots.Add(new ProbabilitySlotSoltion()
                    {
                        start = lastSlot.end + 1,
                        end = lastSlot.end + slotSize,
                        solution = individual
                    });
                }
                else
                {
                    probabilitySlots.Add(new ProbabilitySlotSoltion()
                    {
                        start = 0,
                        end = slotSize - 1,
                        solution = individual
                    });
                }
            }

            Random rnd = new Random();

            var rndNR = rnd.Next(0, probabilitySlots.Last().end + 1);

            foreach (var probabilitySlot in probabilitySlots)
            {
                if (rndNR >= probabilitySlot.start &&
                    rndNR <= probabilitySlot.end)
                {
                    return probabilitySlot.solution;
                }
            }

            return null;
        }

        private Solution RankSelection()
        {
            // 1.Sort the individs of population from best to worst,
            List<Solution> rankedPopulation = Populate.OrderByDescending(x => x.GetScore()).ToList();
            double slotSizeFactor = 100000;
            var probabilitySlots = new List<ProbabilitySlotSoltion>();
            double sumRank = (p * (p + 1)) / 2;   //2.  SUM of total ranks, F = popsize(popsize+1)/2

            for (int i = 0; i < p; i++)
            {
                int rank_i = i + 1; // assigning rank popsizeto the best, popsize-1 to the next best, etc…, and rank 1 to the worst.
                double propability = rank_i / sumRank;  // 3.Associate a probability Rank_i/F  with each individual i.
                int slotSize = (int)(propability * slotSizeFactor);
                var lastSlot = probabilitySlots.LastOrDefault();

                if (lastSlot != null)
                {
                    probabilitySlots.Add(new ProbabilitySlotSoltion()
                    {
                        start = lastSlot.end + 1,
                        end = lastSlot.end + slotSize,
                        solution = rankedPopulation[i]
                    });
                }
                else
                {
                    probabilitySlots.Add(new ProbabilitySlotSoltion()
                    {
                        start = 0,
                        end = slotSize - 1,
                        solution = rankedPopulation[i]
                    });
                }
            }

            // 4.Using these probabilities, choose one individual X, as roulette wheel, and return X, 
            Random rnd = new Random();
            var rndNR = rnd.Next(0, probabilitySlots.Last().end + 1);
            foreach (var probabilitySlot in probabilitySlots)
            {
                if (rndNR >= probabilitySlot.start &&
                    rndNR <= probabilitySlot.end)
                {
                    return probabilitySlot.solution;
                }
            }
            return null;
        }

        private Solution TournamentSelection()
        {
            List<Solution> TournamentSelectionIndividuals = new List<Solution>();
            List<int> PopulationIndexes = new List<int>();
            for (int i = 0; i < p; i++)
            {
                PopulationIndexes.Add(i);
            }
            Random rnd = new Random();
            for (int i = 0; i < this.t; i++)
            {
                // Taking care (just in case, even though it's rare to happen) of tournament size parameter is greater than population size parameter
                if (i <= p)
                {
                    int rndIndex = rnd.Next(0, PopulationIndexes.Count);
                    int rndIndexAsElement = PopulationIndexes[rndIndex];
                    TournamentSelectionIndividuals.Add(Populate[rndIndexAsElement]);
                    PopulationIndexes.RemoveAt(rndIndex);
                }
            }
            return TournamentSelectionIndividuals.OrderByDescending(x => x.GetScore()).FirstOrDefault();
        }

        private Solution GetWorst()
        {
            Solution worst = Populate[0];

            for (int i = 1; i < this.p; i++)
            {
                if (Populate[i].GetScore() < worst.GetScore())
                {
                    worst = Populate[i];
                }
            }

            return worst;
        }

        private Solution GetBest()
        {
            Solution best = Populate[0];

            for (int i = 1; i < this.p; i++)
            {
                if (Populate[i].GetScore() > best.GetScore())
                {
                    best = Populate[i];
                }
            }

            return best;
        }
    }
}

class ProbabilitySlotSoltion
{
    public int start;
    public int end;
    public Solution solution;
}
