﻿using RouterPlacement.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RouterPlacement
{
    class HillCliming
    {
        public static Solution FindSolution()
        {
            var currentSolution = Solution.GenerateRandom();
            IO.PlotSolution(currentSolution);

            for (int i = 0; i < 100; i++)
            {
                var mutation = currentSolution.Copy();
                mutation.Mutate();
                Console.WriteLine($"Index: {i}");

                if (currentSolution.GetScore() <= mutation.GetScore())
                {
                    Console.WriteLine(mutation.GetScore());
                    currentSolution = mutation;
                    i = 0;
                }
                else
                {
                    var random = new Random().Next(1, 11);
                    // get worse solution with probalility 10%
                    if (random == 1)
                    {
                        currentSolution = mutation;
                    }
                }
            }

            IO.PlotSolution(currentSolution);

            return currentSolution;
        }
    }
}
