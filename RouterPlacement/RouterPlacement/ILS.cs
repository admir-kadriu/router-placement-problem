﻿using RouterPlacement.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RouterPlacement
{
    class ILS
    {
        readonly int iterations;

        public ILS(int iterations = 20000)
        {
            this.iterations = iterations;
        }

        public  Solution FindSolution()
        {
            DateTime startTime = DateTime.Now;
            Random rnd = new Random();           
            List<int> T = new List<int>() {100, 120, 115, 70, 85, 90 };
            Solution S = Solution.GenerateRandom();
            Solution H = S.Copy();
            Solution Best = H.Copy();

            //TODO: GR 6 assigment, remove iterations work only with time

            while (!TimeDifferenceReached(startTime))
            {
                DateTime startTimeInner = DateTime.Now;
                int time = T[rnd.Next(T.Count)];

                while (!TimeDifferenceReached(startTime) && !TimeDifferenceReachedSeconds(startTimeInner, time))
                {
                    var R = S.Copy();
                    R.Mutate();
                    var random = rnd.Next(1, 6);
                    if (S.GetScore() < R.GetScore() || random == 1)
                    {                        
                        S = R;
                    }                                                          
                }

                if(S.GetScore() > Best.GetScore())
                {
                    Best = S;
                }

                H = NewHomeBase(H, S);
                S = H.Copy();
                S.Perturb();               
            }
            return Best;
        }

        public Solution NewHomeBase(Solution H, Solution S)
        {
            return S.GetScore() >= H.GetScore() ? S : H;
        }
        
        private bool TimeDifferenceReached(DateTime startTime, int total_execution_minutes = 2)
        {
            bool result = (int)DateTime.Now.Subtract(startTime).TotalMinutes > total_execution_minutes ? true : false;
            return result;
        }

        private bool TimeDifferenceReachedSeconds(DateTime startTime, int total_execution_seconds = 60)
        {
            bool result = (int)DateTime.Now.Subtract(startTime).TotalSeconds > total_execution_seconds ? true : false;
            return result;
        }
    }
}
