﻿using RouterPlacement.Enums;
using RouterPlacement.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Drawing.Drawing2D;

namespace RouterPlacement
{
    class IO
    {
        public static void Read(string filePath)
        {
            int i = 0;
            string line;
            StreamReader streamReader = new StreamReader(filePath);

            while ((line = streamReader.ReadLine()) != null)
            {
                String[] substr = line.Split(' ');


                if (i == 0)
                {
                    Building.RowCount = int.Parse(substr[0]);
                    Building.ColumnCount = int.Parse(substr[1]);
                    Router.Radius = int.Parse(substr[2]);

                    Building.Planimetry = new char[Building.RowCount, Building.ColumnCount];
                }
                else if (i == 1) /*Second line - details for budget*/
                {
                    Building.BackBoneCost = int.Parse(substr[0]);
                    Router.UnitCost = int.Parse(substr[1]);
                    Building.InfrastructureBudget = int.Parse(substr[2]);
                }
                else if (i == 2) /*Third line - backbone coordinates*/
                {
                    var row = int.Parse(substr[0]);
                    var column = int.Parse(substr[1]);

                    Building.BackBoneCell = new Cell(row, column, true);
                }
                else /*Other lines - planimetry*/
                {
                    int j = 0;
                    foreach (char character in line)
                    {
                        Building.Planimetry[i - 3, j] = character;

                        if ((CellType)character == CellType.Target)
                        {
                            var cell = new Cell(i - 3, j);
                            Building.TargetCells.Add(cell.Id, cell);
                        }
                        j++;
                    }
                }
                i++;
            }

            streamReader.Close();

            return;
        }
        public static void PlotPlanimetry()
        {
            for (int i = 0; i < Building.RowCount; i++)
            {
                for (int j = 0; j < Building.ColumnCount; j++)
                {
                    Console.Write(string.Format("{0} ", Building.Planimetry[i, j]));
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
        }

        /// <summary>
        /// Plots a solution on a 2D space.
        /// 1: Plots wall, target and void cells
        /// 2: Plots covered cells
        /// 3: Plots cells connected to backbone
        /// 4: Plots routers
        /// </summary>
        /// <param name="solution"></param>
        public static void PlotSolution(Solution solution, int scale = 10)
        {
            Bitmap map = new Bitmap(Building.ColumnCount, Building.RowCount);

            for (int i = 0; i < Building.RowCount; i++)
            {
                for (int j = 0; j < Building.ColumnCount; j++)
                {
                    if (Building.Planimetry[i, j] == '#')
                    {
                        map.SetPixel(j, i, Color.Black);
                    }
                    else if (Building.Planimetry[i, j] == '.')
                    {
                        map.SetPixel(j, i, Color.Gray);
                    }
                    else
                    {
                        map.SetPixel(j, i, Color.Brown);
                    }
                }
            }

            foreach (var router in solution.Routers)
            {
                foreach (var cell in router.GetCoveredCells())
                {
                    map.SetPixel(cell.Column, cell.Row, Color.Yellow);
                }
            }

            foreach (var cell in solution.ConnectedCells)
            {
                map.SetPixel(cell.Column, cell.Row, Color.CadetBlue);
            }

            foreach (var router in solution.Routers)
            {
                map.SetPixel(router.Cell.Column, router.Cell.Row, Color.Blue);
            }

            // Make a bitmap of the right size.
            int newWidth = map.Width * scale;
            int newHeight = map.Height * scale;
            Bitmap scaled = new Bitmap(newWidth, newHeight);

            // Draw the image onto the new bitmap.
            using (Graphics gr = Graphics.FromImage(scaled))
            {
                // No smoothing.
                gr.InterpolationMode = InterpolationMode.NearestNeighbor;

                Point[] dest =
                {
                    new Point(0, 0),
                    new Point(newWidth, 0),
                    new Point(0, newHeight),
                };

                Rectangle source = new Rectangle(
                    0, 0,
                    map.Width,
                    map.Height);

                gr.DrawImage(map,
                    dest, source, GraphicsUnit.Pixel);
            }

            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            int secondsSinceEpoch = (int)t.TotalSeconds;

            string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string fileName = Path.Combine(path, "Output\\solution"+ secondsSinceEpoch.ToString() + ".png");

            scaled.Save(fileName, ImageFormat.Png);
        }
    }
}
