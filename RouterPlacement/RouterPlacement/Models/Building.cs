﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RouterPlacement.Models
{
    class Building
    {
        public static char[,] Planimetry { get ; set ; }
        public static int ColumnCount { get ; set; }
        public static int RowCount { get; set; }

        public static int InfrastructureBudget { get; set; }
        public static int BackBoneCost { get; set; }

        public static Cell BackBoneCell { get; set; }

        // We store target cells here to access them more easly
        public static Dictionary<string, Cell> TargetCells { get; set; } = new Dictionary<string, Cell>();
    }
}
