﻿using RouterPlacement.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace RouterPlacement.Models
{
    class Cell
    {
        public string Id { get; }
        public int Row { get; }
        public int Column { get; }
        public double DistanceToBackbone { get; set; }

        public Cell(int row, int column, bool isBackboneCell = false)
        {
            this.Row = row;
            this.Column = column;
            this.Id = this.Row + ":" + this.Column;
            this.DistanceToBackbone = !isBackboneCell ? GetDistanceToCell(Building.BackBoneCell) : 0;
        }

        public new CellType GetType()
        {
            return (CellType)Building.Planimetry[this.Row, this.Column];
        }

        /// <summary>
        /// Returns a list of cells between two cells.
        /// Inculdes cellToConnect and this cell
        /// </summary>
        /// <param name="cellToConnect"></param>
        /// <returns></returns>
        public List<Cell> GetPathToCell(Cell cellToConnect)
        {
            List<Cell> cells = new List<Cell>();


            var row = cellToConnect.Row;
            var column = cellToConnect.Column;

            while (this.Row != row || this.Column != column)
            {
                Cell cell = null;

                if (row == this.Row)
                {
                    if (column < this.Column)
                        cell = new Cell(row, column + 1);
                    else
                        cell = new Cell(row, column - 1);
                }
                else if (column == this.Column)
                {
                    if (row < this.Row)
                        cell = new Cell(row + 1, column);
                    else
                        cell = new Cell(row - 1, column);
                }
                else if (column < this.Column)
                {
                    if (row < this.Row)
                        cell = new Cell(row + 1, column + 1);
                    else
                        cell = new Cell(row - 1, column + 1);
                }
                else
                {
                    if (row < this.Row)
                        cell = new Cell(row + 1, column - 1);
                    else
                        cell = new Cell(row - 1, column - 1);
                }

                cells.Add(cell);

                row = cell.Row;
                column = cell.Column;
            }

            cells.Add(cellToConnect);

            return cells;
        }

        /// <summary>
        /// Returns true if this and cell contains wall in the smallest closing rectangle
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public bool WallExistsInBetween(Cell cell)
        {
            var top = Math.Min(this.Row, cell.Row);
            var bottom = Math.Max(this.Row, cell.Row);

            var left = Math.Min(this.Column, cell.Column);
            var right = Math.Max(this.Column, cell.Column);

            for (int i = top; i <= bottom; i++)
            {
                for (int j = left; j <= right; j++)
                {
                    if (new Cell(i, j).GetType() == CellType.Wall)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns euclidean distance between this and cell
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public double GetDistanceToCell(Cell cell)
        {
            return Math.Sqrt(
                Math.Pow((this.Column - cell.Column), 2) +
                Math.Pow((this.Row - cell.Row), 2));
        }
    }
}
