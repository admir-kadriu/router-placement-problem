﻿using RouterPlacement.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RouterPlacement.Models
{
    class Router
    {
        public static int Radius;
        public static int UnitCost;
        public Cell Cell { get; set; }

        public Router(Cell cell)
        {
            if(cell.GetType() == CellType.Wall)
            {
                throw new Exception("Router placed on wall cell");
            }

            this.Cell = cell;
        }

        /// <summary>
        /// Return target cells that are inside router radius.
        /// Makes sure there is no wall in between each cell and the router.
        /// </summary>
        /// <returns></returns>
        public List<Cell> GetCoveredCells()
        {
            List<Cell> coveredCells = new List<Cell>();

            var top = this.Cell.Row - Router.Radius;
            var bottom = this.Cell.Row + Router.Radius;
            var left = this.Cell.Column - Router.Radius;
            var right = this.Cell.Column + Router.Radius;

            if (top < 0)
                top = 0;

            if (bottom >= Building.RowCount)
                bottom = Building.RowCount - 1;

            if (left < 0)
                left = 0;

            if (right >= Building.ColumnCount)
                right = Building.ColumnCount - 1;

            for (int i = top; i <= bottom; i++)
            {
                for (int j = left; j <= right; j++)
                {
                    var cell = new Cell(i, j);
                    if(cell.GetType() == CellType.Target && !this.Cell.WallExistsInBetween(cell)) 
                    {
                        coveredCells.Add(cell);
                    }
                }
            }

            return coveredCells;
        }

        /// <summary>
        /// Find the closest cell connected to backbone and return best path to reach it
        /// (We can get backbone internet from any cell that currently is connected to backbone)
        /// </summary>
        /// <param name="connectedCells"></param>
        /// <returns></returns>
        public List<Cell> GetBestPathToBackbone(List<Cell> connectedCells)
        {
            Cell nearestCell = Building.BackBoneCell;
            var nearestCellDistance = this.Cell.GetDistanceToCell(nearestCell);

            foreach (var cell in connectedCells)
            {
                var currentCellDistnce = this.Cell.GetDistanceToCell(cell);
                if(currentCellDistnce < nearestCellDistance)
                {
                    nearestCell = cell;
                    nearestCellDistance = currentCellDistnce;
                }
            }

            return this.Cell.GetPathToCell(nearestCell);
        }

        public List<Cell> GetWallCells()
        {
            List<Cell> wallCells = new List<Cell>();

            int startRowIndex = (Cell.Row - Radius) < (Building.RowCount - 1) ? (Cell.Row - Radius) : 0;
            int endRowIndex = (Cell.Row + Radius) > (Building.RowCount - 1) ? (Building.RowCount - 1) : (Cell.Row + Radius);
            int startColumnIndex = (Cell.Column - Radius) < (Building.ColumnCount - 1) ? (Cell.Column - Radius) : 0;
            int endColumnIndex = (Cell.Column + Radius) > (Building.ColumnCount - 1) ? (Building.ColumnCount - 1) : (Cell.Column + Radius);

            for (int i = startRowIndex; i <= endRowIndex; i++)
            {
                for (int j = startColumnIndex; j <= endColumnIndex; j++)
                {
                    if ((CellType)Building.Planimetry[i, j] == CellType.Wall)
                    {
                        wallCells.Add(new Cell(i, j));
                    }
                }
            }
            return wallCells;
        }

        public bool IsCovered(int routerX, int routerY, int targetX, int targetY, List<Cell> wallCells)
        {
            if(wallCells.Any(x => 
                x.Row >= Math.Min(routerX, targetX) && x.Row <= Math.Max(routerX, targetX) &&
                x.Column >= Math.Min(routerY, targetY) && x.Column <= Math.Max(routerY, targetY)))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns and estimated count of how many routers the bugget allows
        /// </summary>
        /// <returns></returns>
        public static double EstimatedTotalRouters()
        {
            return Math.Floor((double)(Building.InfrastructureBudget / Router.UnitCost));
        }
    }
}
