﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RouterPlacement.Models
{
    class Solution
    {
        public List<Cell> ConnectedCells { get; set; }
        public List<Router> Routers { get; set; }
        public HashSet<string> CoveredCells { get; set; }
        public Guid Id { get; }
        public Dictionary<string, Router> UniqueCellsCoveredByRouters { get; set; }

        public Solution()
        {
            this.Id = Guid.NewGuid();
            this.Routers = new List<Router>();
            this.ConnectedCells = new List<Cell>();
            this.CoveredCells = new HashSet<string>();
            this.UniqueCellsCoveredByRouters = new Dictionary<string, Router>();
        }

        public static Solution GenerateRandom()
        {
            var solution = new Solution();

            var random = new Random();

            var targetCells = new Dictionary<string, Cell>(Building.TargetCells);

            var routerBudget = 0.95 * Building.InfrastructureBudget;

            var generatedRandomRouters = new List<Router>();

            while (routerBudget >= Router.UnitCost && targetCells.Count > 0)
            {
                var rnd = random.Next(targetCells.Count - 1);

                var idList = targetCells.Keys.ToList();
                var id = idList[rnd];
                var router = new Router(targetCells[id]);

                generatedRandomRouters.Add(router);

                foreach (Cell cell in router.GetCoveredCells())
                {
                    targetCells.Remove(cell.Id);
                }

                targetCells.Remove(router.Cell.Id);

                routerBudget -= Router.UnitCost;
            }

            generatedRandomRouters = generatedRandomRouters.OrderBy(x => x.Cell.DistanceToBackbone).ToList();

            for (int i = 0; i < generatedRandomRouters.Count; i += 2)
            {
                solution.AddRouter(generatedRandomRouters[i]);
            }

            for (int i = 1; i < generatedRandomRouters.Count; i += 2)
            {
                solution.AddRouter(generatedRandomRouters[i]);
            }

            return solution;
        }

        /// <summary>
        /// Returns the cost of the current solution
        /// </summary>
        /// <returns></returns>
        public int GetCost()
        {
            return this.Routers.Count * Router.UnitCost + this.ConnectedCells.Count * Building.BackBoneCost;
        }

        public bool AddRouter(Router router)
        {
            List<Cell> cellsToConnect = new List<Cell>();

            cellsToConnect = router.GetBestPathToBackbone(this.ConnectedCells);

            var costToAdd = cellsToConnect.Count * Building.BackBoneCost + Router.UnitCost;

            if ((this.GetCost() + costToAdd) <= Building.InfrastructureBudget)
            {
                this.Routers.Add(router);
                this.ConnectedCells.AddRange(cellsToConnect);
                var coveredCells = router.GetCoveredCells();

                foreach (var cell in coveredCells)
                {
                    this.CoveredCells.Add(cell.Id);

                    if (UniqueCellsCoveredByRouters.ContainsKey(cell.Id))
                    {
                        UniqueCellsCoveredByRouters.Remove(cell.Id);
                    }
                    else
                    {
                        UniqueCellsCoveredByRouters.Add(cell.Id, router);
                    }
                }
                return true;
            }

            return false;
        }


        public int GetScore()
        {
            int score = 0;
            score = Building.InfrastructureBudget - (this.Routers.Count * Router.UnitCost) - (this.ConnectedCells.Count * Building.BackBoneCost);
            score += 1000 * CoveredCells.Count;
            return score;
        }

        /// <summary>
        /// Removes all currently connected cells.
        /// Connects all routers to the backbone.
        /// </summary>
        public void ReconnectRoutersToBackbone()
        {
            var routers = new List<Router>(this.Routers);

            this.ConnectedCells = new List<Cell>();
            this.Routers = new List<Router>();
            this.CoveredCells = new HashSet<string>();
            this.UniqueCellsCoveredByRouters = new Dictionary<string, Router>();

            foreach (var router in routers)
            {
                var routerIsAdded = this.AddRouter(router);

                if (!routerIsAdded)
                {
                    break;
                }
            }

            return;
        }

        /// <summary>
        /// Removes a random router and inserts an other random router. Creating 
        /// the ilusion tha a random router has jumped in an other part of the planimetry.
        /// </summary>
        public void RouterJumpMutation()
        {
            //delete a random router
            var random = new Random();
            if (this.Routers.Count() <= 1)
                return;
            int rnd = random.Next(this.Routers.Count() - 1);

            this.Routers.RemoveAt(rnd);

            var idList = Building.TargetCells.Keys.ToList();
            rnd = random.Next(idList.Count - 1);
            var id = idList[rnd];

            var router = new Router(Building.TargetCells[id]);

            this.Routers.Add(router);

            this.ReconnectRoutersToBackbone();

            return;
        }

        /// <summary>
        /// delete a random router
        /// </summary>
        public void RemoveRouterMutation()
        {
            var random = new Random();
            if (this.Routers.Count() <= 1)
                return;
            int rnd = random.Next(this.Routers.Count() - 1);
            this.Routers.RemoveAt(rnd);
            this.ReconnectRoutersToBackbone();

            return;
        }

        /// <summary>
        /// Add a random router
        /// </summary>
        public void AddRouterMutation()
        {
            var idList = Building.TargetCells.Keys.ToList();
            var rnd = new Random().Next(idList.Count - 1);
            var id = idList[rnd];

            var router = new Router(Building.TargetCells[id]);

            this.AddRouter(router);
        }

        /// <summary>
        /// Move a router in a near distance
        /// </summary>
        public void ShiftRouterMutation()
        {
            var random = new Random();
            if (this.Routers.Count() <= 1)
                return;

            int rnd = random.Next(this.Routers.Count() - 1);
            var router = this.Routers[rnd];

            int cnt = 2 * Router.Radius;
            var maxRandomRouters = 10;

            while (cnt > 0 && maxRandomRouters > 0)
            {
                var randomShiftRow = random.Next(-Router.Radius, Router.Radius + 1);
                var randomShiftColumn = random.Next(-Router.Radius, Router.Radius + 1);

                var newRow = router.Cell.Row + randomShiftRow;
                var newColumn = router.Cell.Column + randomShiftColumn;

                var id = $"{newRow}:{newColumn}";

                if (Building.TargetCells.ContainsKey(id))
                {
                    this.Routers.RemoveAt(rnd);

                    var cell = new Cell(newRow, newColumn);
                    this.Routers.Add(new Router(cell));
                    this.ReconnectRoutersToBackbone();
                    return;
                }
                cnt--;

                if (cnt == 0)
                {
                    rnd = random.Next(this.Routers.Count() - 1);
                    router = this.Routers[rnd];
                    cnt = 2 * Router.Radius;
                    maxRandomRouters--;
                }
            }
        }

        /// <summary>
        /// Sorts this.Routers by distance to backbone cell
        /// </summary>
        public void SortRouterByDistanceToBackbone()
        {
            this.Routers = this.Routers.OrderBy(x => x.Cell.DistanceToBackbone).ToList();
        }

        /// <summary>
        /// Sorts this.Routers based to router covered cells
        /// </summary>
        public void SortRoutersByCoveredCells()
        {
            var routers = new List<Tuple<Router, int>>();

            foreach (var router in this.Routers)
            {
                var uniqueCellsForRouter = UniqueCellsCoveredByRouters.Where(x => x.Value == router).Count();

                routers.Add(new Tuple<Router, int>(router, uniqueCellsForRouter));
            }

            this.Routers = routers.OrderByDescending(x => x.Item2).Select(x => x.Item1).ToList();
        }

        public Solution Copy()
        {
            var solution = new Solution
            {
                Routers = new List<Router>(this.Routers),
                ConnectedCells = new List<Cell>(this.ConnectedCells),
                CoveredCells = new HashSet<string>(this.CoveredCells),
                UniqueCellsCoveredByRouters = new Dictionary<string, Router>(this.UniqueCellsCoveredByRouters)
            };

            return solution;
        }

        public void Mutate()
        {
            var random = new Random().Next(1, 11);

            if (random <= 5)
            {
                ShiftRouterMutation();
            }
            else if (random <= 6)
            {
                RemoveRouterMutation();
            }
            else if (random <= 7)
            {
                AddRouterMutation();
            }
            else
            {
                RouterJumpMutation();
            }
        }

        public void Perturb(int percentage = 30, int router_placement_tries = 50)
        {
            var random = new Random();
            int removed_count = this.Routers.Count * percentage / 100;
            int rnd;
            int i;
            for (i = 0; i < removed_count; i++)
            {
                rnd = random.Next(this.Routers.Count() - 1);
                this.Routers.RemoveAt(rnd);
            }
            this.ReconnectRoutersToBackbone();

            for (i = 0; i < removed_count; i++)
            {
                Router router;
                int index = random.Next(Building.TargetCells.Count - 1);
                var idList = Building.TargetCells.Keys.ToList(); ;
                var id = idList[index];
                Cell router_cell;
                int tries = 0;
                do
                {
                    index = random.Next(Building.TargetCells.Count - 1);
                    idList = Building.TargetCells.Keys.ToList();
                    id = idList[index];
                    router_cell = Building.TargetCells[id];
                    router = new Router(router_cell);
                    tries++;
                } while (!this.AddRouter(router) && tries < router_placement_tries);
            }

        }
    }
}