﻿using RouterPlacement.Models;
using System;

namespace RouterPlacement
{
    class Program
    {
        static void Main(string[] args)
        {
            IO.Read("Input/sample.in");

            GeneticAlgorithm ga = new GeneticAlgorithm(100, 3, 80, 10000);

            var solution = ga.FindSolution();

            IO.PlotSolution(solution);            
            Console.WriteLine("Pres any key to exit...");
            Console.ReadKey();
        }
    }
}
